/* global chalk */
/* global yosay */
'use strict';
var generators = require('yeoman-generator'),
   _ = require('lodash'),
   chalk = require('chalk'),
   yosay = require('yosay');
module.exports = generators.Base.extend({
    constructor: function () {
        generators.Base.apply(this, arguments);
        this.argument('appname',{type:String, required:true});
        this.appname = _.kebabCase(this.appname);
        this.log(this.appname);
        
        this.option('includeutils',{
            desc:"include angular utils",
            type:Boolean,
            default:false
        });
    },
    initializing: {
    },
    prompting: function(){
        this.log(this.appname);
        this.log(yosay(
            "Welcome to" +
            chalk.yellow(' WebApp') + " generator"));
            
         var done = this.async();
         this.prompt([{
             type:'input',
             name:'ngappname',
             message:'Write yor app name(ng-app)',
             default: this.config.get("ngappname") || 'app'
         },
         {
             type:'checkbox',
             name:'jslibs',
             message:'What to install?',
             choices:[
                 {
                     name:'lodash',
                     value:'lodash',
                     checked:true
                 },
                 {
                     name:'Moment.js',
                     value:'momentjs',
                     checked:true
                 },
                 {
                     name:'Angular-Ui utils',
                     value:'angularuiutils',
                     checked:true
                 }
             ]
         }],
         function(answers){
             //this.ngappname = answers.ngappname;
             this.log(answers);
             this.config.set("ngappname", _.kebabCase(answers.ngappname));
             this.config.save();
             this.includeLodash = _.includes(answers.jslibs,'lodash');
             this.includeMoment = _.includes(answers.jslibs,'momentjs');
             this.includeAngularUtils = _.includes(answers.jslibs,'angularuiutils');
             done();
         }.bind(this));   
    },
    configuring: function(){
    },
    writing:{
        gulpfile: function(){
            this.copy('_gulpfile.js','gulpfile.js');
            this.copy('_gulp.config.js','gulp.config.js');
            this.copy('jshintrc','.jshintrc');
        },
        packacgeJSON:function(){
            this.copy('_package.json','package.json');
        },
        git:function(){
            this.copy('gitignore','.gitignore');
        },
        bower: function(){
            var bowerJson = {
                name: _.kebabCase(this.appname), 
                license: 'MIT',
                dependencies: {}
            };
            bowerJson.dependencies['angular'] = '~1.4.6';
            bowerJson.dependencies['angular-bootstrap'] = '~0.13.4';
            bowerJson.dependencies['angular-ui-router'] = '~0.2.15';
            bowerJson.dependencies['bootstrap-css-only'] = '~3.3.5';
            if(this.includeLodash){
                bowerJson.dependencies['lodash'] = '~3.10.1';
            }
            if(this.includeMoment){
                bowerJson.dependencies['moment'] = '~2.10.6';
            }
            if(this.includeAngularUtils){
                bowerJson.dependencies['angular-ui-utils'] = '~3.0.0';
            }
           /* if(this.options.includeutlis){
                
            }*/
            this.fs.writeJSON('bower.json', bowerJson);

            this.copy('bowerrc', '.bowerrc');
        },
        appStaticFiles:function(){
            //var source = this.templatePath("_favicon.ico");
            //var destination = this.destinationPath("src/favicon.ico");
            this.copy("_favicon.ico","src/favicon.ico");
            this.copy("_tsd.json","tsd.json");
            this.directory('styles','src/styles');
        },
        scripts:function(){
            this.fs.copyTpl(
            this.templatePath('app/_app.js'),
            this.destinationPath('src/app/_app.js'),
                {
                    ngapp:this.config.get("ngappname")
                }
            );
            this.fs.copyTpl(
            this.templatePath('app/layout/_shell.controller.js'),
            this.destinationPath('src/app/layout/shell.controller.js'),
                {
                    ngapp:this.config.get("ngappname")
                }
            );
            this.fs.copyTpl(
            this.templatePath('app/home/_home.controller.js'),
            this.destinationPath('src/app/home/home.controller.js'),
                {
                    ngapp:this.config.get("ngappname")
                }
            );
            this.fs.copyTpl(
            this.templatePath('app/about/_about.controller.js'),
            this.destinationPath('src/app/about/about.controller.js'),
                {
                    ngapp:this.config.get("ngappname")
                }
            );
        },
        html:function(){
            this.fs.copyTpl(
            this.templatePath('_index.html'),
            this.destinationPath('src/index.html'),
                {
                    appname: _.startCase(this.appname),
                    ngapp:this.config.get("ngappname")
                }
            );
            this.fs.copyTpl(
            this.templatePath('app/about/_about.html'),
            this.destinationPath('src/app/about/about.html')
            );
            this.fs.copyTpl(
            this.templatePath('app/layout/_shell.html'),
            this.destinationPath('src/app/layout/shell.html')
            );
            this.fs.copyTpl(
            this.templatePath('app/home/_home.html'),
            this.destinationPath('src/app/home/home.html')
            );
        }

    },
    conflicts: function(){
    },
    install: function(){
        this.installDependencies({
            skipInstall:this.options['skip-install'],
        });
        if(!this.options['skip-install']){
            this.spawnCommand('tsd',['install']);
        }
    },
    end: function(){
        this.log(chalk.yellow.bold("SetUp is done!"));
        var message =
        chalk.yellow.bold('Installation is finished') 
        + 'to run add scripts and css run gulp'
        + chalk.yellow.bold('gulp wiredep');
        
        if(this.options['skip-install']){
            this.log(message);
            return;
        }
        
    }

});
