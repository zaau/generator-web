'use strict';

var generators = require('yeoman-generator');

var MyBase= generators.Base.extend({
    anotherHelper: function(){
        console.log("inside another helper");
    }
});

module.exports = MyBase.extend({
    constructor: function () {
        generators.Base.apply(this, arguments);
        this.argument('name',{type:String, required:false});
    },
    init:function(){
        console.log("inside init");
        this.baz = function(){
            console.log("inside baz");
        }
    },
    _foo: function (){
        console.log("inside foo");
    },
    bar: function(){
        console.log("inside bar");
        this._foo();
        this.baz();
        this.anotherHelper();
    }
});